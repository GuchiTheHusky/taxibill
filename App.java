package org.example.modulFive.lessonThree.taxi;

import java.util.Scanner;

public class App
{
    public static void showInfo(){
        System.out.println("Input:" +
                "\n < 1 > Taxi type;" +
                "\n < 2 > Distance;" +
                "\n < 3 > Price;" +
                "\n < 4 > Baggage;" +
                "\n < 5 > Pets;" +
                "\n < 6 > Bill;" +
                "\n < 7 > Exit.");
    }
    public static void error(){
        System.out.println("Error, wrong number.");
    }
    public static String [] taxiType = {"Economic  2./1km", "Business  5/1km", "Luxury  10/1km"};
    public static void showTaxiType(String[] array){
        for (int i = 0; i < taxiType.length; i++) {
            System.out.println(i + 1 + ": " + taxiType[i]);

        }
    }
    public static void main( String[] args ){
        Scanner scan = new Scanner(System.in);
        Taxi taxi = new Taxi();

        boolean isAlive = true;
        int choice = 0;

        do {
            showInfo();
            do {
                choice = scan.nextInt();
                if (choice < 1 || choice > 7) showInfo();
            }
            while (choice < 1 || choice > 7);

            switch (choice){
                case 1:{
                    System.out.println("Select your choice:");
                    showTaxiType(taxiType);
                    scan = new Scanner(System.in);
                    int temp = scan.nextInt();
                    if (temp < 1 || temp > 3) {
                        error();
                    }
                    else {
                        if (temp == 1){
                            taxi.setType(taxiType[0]);
                        }
                        else if (temp == 2){
                            taxi.setType(taxiType[1]);
                        }
                        else {
                            taxi.setType(taxiType[2]);
                        }
                        System.out.println("Excellent choice.");
                    }
                }
                break;
                case 2:{
                    if (!taxi.checkTaxiType()){
                        System.out.println("First choose taxi type.");
                    }
                    else {
                        scan = new Scanner(System.in);
                        System.out.println("Input distance: ");
                        int distance = scan.nextInt();
                        if (distance <= 0){
                            error();
                        }
                        else {
                            taxi.setDistance(distance);
                        }
                    }
                }
                break;
                case 3:{

                    if (!taxi.checkTaxiType()){
                        System.out.println("First choose taxi type.");
                    }
                    else {
                        System.out.println("Input price for call: ");
                        scan = new Scanner(System.in);
                        int priceForCall = scan.nextInt();
                        if (priceForCall < 0){
                            error();
                        }
                        else {
                            taxi.setPriceForCall(priceForCall);
                        }
                    }
                }
                break;
                case 4:{
                    if (!taxi.checkTaxiType()){
                        System.out.println("First choose taxi type.");
                    }
                    else {
                        System.out.println("Input baggage count (1...50kg), price for one kg 0.5$: ");
                        scan = new Scanner(System.in);
                        int baggageCount = scan.nextInt();
                        if (baggageCount <= 0 || baggageCount > 50){
                            error();
                        }
                        else {
                            taxi.setBaggage(baggageCount);
                        }
                    }
                }
                break;
                case 5:{
                    if (!taxi.checkTaxiType()){
                        System.out.println("First choose taxi type.");
                    }
                    else {
                        System.out.println("Input pets count (0...5), price for one pet 10$.");
                        scan = new Scanner(System.in);
                        int petsCount = scan.nextInt();
                        if (petsCount < 0 || petsCount > 5){
                            error();
                        }
                        else {
                            taxi.setPetsCount(petsCount);
                        }
                    }
                }
                break;
                case 6:{
                    if (!taxi.checkTaxiType() || !taxi.checkDistance()){
                        System.out.println("First choose taxi type & distance.");
                    }
                    else {
                        System.out.println("Price for your trip is:");
                        System.out.println(taxi.bill() + "$");
                    }
                }
                break;
                case 7:{
                    System.out.println("Exit.");
                    isAlive = false;
                }
            }
        }
        while (isAlive);
    }
}
