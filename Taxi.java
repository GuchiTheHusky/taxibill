package org.example.modulFive.lessonThree.taxi;

public class Taxi {
    private int priceForCall;
    private int distance;
    private int baggage;
    private int petsCount;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getBaggage() {
        return baggage;
    }

    public void setBaggage(int baggage) {
        this.baggage = baggage;
    }

    public int getPetsCount() {
        return petsCount;
    }

    public void setPetsCount(int petsCount) {
        this.petsCount = petsCount;
    }

    public int getPriceForCall() {
        return priceForCall;
    }

    public void setPriceForCall(int priceForCall) {
        this.priceForCall = priceForCall;
    }

    public Taxi() {
        priceForCall = 0;
        distance = 0;
        baggage = 0;
        petsCount = 0;
        type = "";
    }

    public Taxi(int priceForCall, int distance, int baggage, int petsCount, String type) {
        this.priceForCall = priceForCall;
        this.distance = distance;
        this.baggage = baggage;
        this.petsCount = petsCount;
        this.type = type;
    }

    public boolean checkTaxiType(){
        return type.length() > 0;
    }
    public boolean checkDistance(){
        return distance > 0;
    }
    private int priceForPets(){
        int temp = petsCount * 10;
        return temp;
    }
    private double priceForBaggage(){
        double temp = baggage * 0.5;
        return temp;
    }
    private int priceForKM(String type){
        if (type.contains("Economic")){
            return 2;
        }
        else if (type.contains("Business")){
            return 5;
        }
        else return 10;
    }
    public int bill(){
        int temp = priceForCall + (distance * priceForKM(type)) + priceForPets() + (int) priceForBaggage();
        return temp;
    }

}
